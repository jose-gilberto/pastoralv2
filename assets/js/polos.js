$(".button-collapse").sideNav();

function listar() {
    $.ajax({
        type: 'GET',
        url: "http://localhost/pastoralv2/polos/listar",
        contentType: "application/json; charset=utf-8",
        success:function (retorno) {
            var polos = JSON.parse(retorno);

            $("#corpo-tabela").html("");
            $.each(polos, function (i, polo) {
                var item =  "<tr>" +
                    "<td>"+ polo.nome +"</td>" +
                    "<td class='blue-text text-darken-3'>" +
                    "<a href='http://localhost/pastoralv2/polos/info/"+ polo.id +"'><i class='material-icons small'>create</i></a>" +
                    "<a href='#' onclick='$(this).excluir("+ polo.id +")'><i class='material-icons small'>delete</i></a>" +
                    "</td>" +
                    "</tr>";
                $("#corpo-tabela").append(item);
            });
        },
        error:function () {
            Materialize.toast('Ocorreu um erro ao listar os Pólos!', 4000, 'red darken-3');
        }
    });
}

$.fn.excluir = function (id) {

    $.ajax({
        type: 'GET',
        url: 'http://localhost/pastoralv2/polos/excluir/'+id,
        success:function () {
            Materialize.toast('Pólo excluído com sucesso!', 4000, 'green darken-3');
            listar();
        },
        error:function () {
            Materialize.toast('Ocorreu um erro ao excluir o Pólo!', 4000, 'red darken-3');
        }
    });

};

$(function () {

    function listarPolos() {

        $.ajax({
            type: 'GET',
            url: "http://localhost/pastoralv2/polos/listar",
            contentType: "application/json; charset=utf-8",
            success:function (retorno) {
                var polos = JSON.parse(retorno);

                $("#corpo-tabela").html("");
                $.each(polos, function (i, polo) {
                    var item =  "<tr>" +
                                    "<td>"+ polo.nome +"</td>" +
                                    "<td class='blue-text text-darken-3'>" +
                                        "<a href='http://localhost/pastoralv2/polos/info/"+ polo.id +"'><i class='material-icons small'>create</i></a>" +
                                        "<a href='#' onclick='$(this).excluir("+ polo.id +")'><i class='material-icons small'>delete</i></a>" +
                                    "</td>" +
                                "</tr>";
                    $("#corpo-tabela").append(item);
                });
            },
            error:function () {
                Materialize.toast('Ocorreu um erro ao listar os Pólos!', 4000, 'red darken-3');
            }
        });
    }

    listarPolos();

    $("#formPolo").bind('submit', function (e) {
        e.preventDefault();

        var nome = $("#nome").val();
        var dados = $(this).serialize();

        $.ajax({
            type:'POST',
            url:"http://localhost/pastoralv2/polos/adicionar",
            data:dados,
            success:function (status) {
                if (status == true) {
                    Materialize.toast('Pólo adicionado com sucesso!', 4000, 'green darken-3');
                    $("#nome").val("");
                    //Chamar lista
                    listarPolos();
                } else {
                    Materialize.toast('Já existe um pólo com esse nome!', 4000, 'yellow darken-2');
                }
            },
            error:function () {
                Materialize.toast('Ocorreu um erro ao adicionar o Pólo!', 4000, 'red darken-3');
            }
        });
    });

    $("#btnVoltar").bind('click', function () {
        $("#nome").val("");
    });

});