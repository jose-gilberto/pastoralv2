$(".button-collapse").sideNav();

function listar() {
    $.ajax({
        type: 'GET',
        url: "http://localhost/pastoralv2/padroeiros/listar",
        contentType: "application/json; charset=utf-8",
        success:function (retorno) {
            var padroeiros = JSON.parse(retorno);

            $("#corpo-tabela").html("");
            $.each(padroeiros, function (i, padroeiro) {
                var data_split = padroeiro.data_festa.split("-");
                var data_festa = data_split[2] + '/' + data_split[1];
                // TODO: reorganizar data
                var item =  "<tr>" +
                    "<td>"+ padroeiro.nome +"</td>" +
                    "<td>"+ data_festa +"</td>" +
                    "<td class='blue-text text-darken-3'>" +
                    "<a href='http://localhost/pastoralv2/padroeiros/info/"+ padroeiro.id +"'><i class='material-icons small'>create</i></a>" +
                    "<a href='#' onclick='$(this).excluir("+ padroeiro.id +")'><i class='material-icons small'>delete</i></a>" +
                    "</td>" +
                    "</tr>";
                $("#corpo-tabela").append(item);
            });
        },
        error:function () {
            Materialize.toast('Ocorreu um erro ao listar os Padroeiros!', 4000, 'red darken-3');
        }
    });
}

$(function () {

    function getMes(str) {
        if (str == "01") {
            return "Janeiro";
        } else if (str == "02") {
            return "Fevereiro";
        } else if (str == "03") {
            return "Março";
        } else if (str == "04") {
            return "Abril";
        } else if (str == "05") {
            return "Maio";
        } else if (str == "06") {
            return "Junho";
        } else if (str == "07") {
            return "Julho";
        } else if (str == "08") {
            return "Agosto";
        } else if (str == "09") {
            return "Setembro";
        } else if (str == "10") {
            return "Outubro";
        } else if (str == "11") {
            return "Novembro";
        } else {
            return "Dezembro";
        }
    }

    function listar() {
        $.ajax({
            type: 'GET',
            url: "http://localhost/pastoralv2/padroeiros/listar",
            contentType: "application/json; charset=utf-8",
            success:function (retorno) {
                var padroeiros = JSON.parse(retorno);

                $("#corpo-tabela").html("");
                $.each(padroeiros, function (i, padroeiro) {
                    var data_split = padroeiro.data_festa.split("-");
                    var data_festa = data_split[2] + ' de ' + getMes(data_split[1]);
                    var item =  "<tr>" +
                        "<td>"+ padroeiro.nome +"</td>" +
                        "<td>"+ data_festa +"</td>" +
                        "<td class='blue-text text-darken-3'>" +
                        "<a href='http://localhost/pastoralv2/padroeiros/info/"+ padroeiro.id +"'><i class='material-icons small'>create</i></a>" +
                        "<a href='#' onclick='$(this).excluir("+ padroeiro.id +")'><i class='material-icons small'>delete</i></a>" +
                        "</td>" +
                        "</tr>";
                    $("#corpo-tabela").append(item);
                });
            },
            error:function () {
                Materialize.toast('Ocorreu um erro ao listar os Padroeiros!', 4000, 'red darken-3');
            }
        });
    }

    listar();

});