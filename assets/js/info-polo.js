$(".button-collapse").sideNav();

$(function () {

    $("#formPolo").bind('submit', function (e) {
        e.preventDefault();

        var nome = $("#nome").val();
        var id = $("#idPolo").val();
        var dados = $(this).serialize();

        $.ajax({
            type:'POST',
            url:"http://localhost/pastoralv2/polos/editar/"+id,
            data:dados,
            success:function (status) {
                if (status == true) {
                    Materialize.toast('Pólo editado com sucesso!', 4000, 'green darken-3');
                    $("#nome").val("");
                    $("#disabled").val(nome);
                } else {
                    Materialize.toast('Já existe um pólo com esse nome!', 4000, 'yellow darken-2');
                }
            },
            error:function () {
                Materialize.toast('Ocorreu um erro ao editar o Pólo!', 4000, 'red darken-3');
            }
        });
    });

    $("#btnVoltar").bind('click', function () {
        $("#nome").val("");
    });

});