<?php
class padroeirosController extends controller {

    // @PATH( BASE_URL/padroeiros/ )
    public function index() {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
        }

        $u = new Usuario();

        $data = array(
            'css' => 'gerenciador.css',
            'js' => 'padroeiros.js',
            'usuario' => $u->getById($_SESSION['pLogin'])
        );

        $this->loadTemplate('padroeiros', $data);
    }

    // @TYPE( POST )
    // @PATH( BASE_URL/padroeiros/adicionar )
    public function adicionar() {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
            exit();
        }

        $p = new Padroeiro();

        if (!empty($_POST['nome']) && !empty($_POST['data'])) {
            $nome = addslashes($_POST['nome']);
            $data = addslashes($_POST['data']);
            $status = $p->adicionar($nome, $data);

            echo $status;
        }
    }


    // @TYPE( GET )
    // @PATH( BASE_URL/padroeiros/listar )
    public function listar() {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
            exit();
        }

        $p = new Padroeiro();

        $retorno = json_encode($p->listar(), JSON_UNESCAPED_UNICODE);
        echo $retorno;
    }

}