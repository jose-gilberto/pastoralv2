<?php
class homeController extends controller {

    public function index() {
        $data = array(
            'css' => 'home.css',
            'js' => 'home.js'
        );
        $this->loadTemplate('home', $data);
    }

}