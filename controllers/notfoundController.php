<?php
class notfoundController extends controller {

    public function index() {
        $data = array(
            'css' => '404.css',
            'js' => '404.js'
        );
        $this->loadTemplate('404', $data);
    }

}