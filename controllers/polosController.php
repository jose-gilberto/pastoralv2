<?php
class polosController extends controller {

    // @PATH( BASE_URL/polos/ )
    public function index() {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
        }

        $u = new Usuario();

        $data = array(
            'css' => 'gerenciador.css',
            'js' => 'polos.js',
            'usuario' => $u->getById($_SESSION['pLogin'])
        );

        $this->loadTemplate('polos', $data);
    }

    // @TYPE( POST )
    // @PATH( BASE_URL/polos/adicionar )
    public function adicionar() {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
            exit();
        }

        $p = new Polo();

        if (!empty($_POST['nome'])) {
            $nome = addslashes($_POST['nome']);
            $status = $p->adicionar($nome);

            echo $status;
        }
    }

    // @TYPE( GET )
    // @PATH( BASE_URL/polos/listar )
    public function listar() {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
            exit();
        }

        $p = new Polo();

        $retorno = json_encode($p->listar(), JSON_UNESCAPED_UNICODE);
        echo $retorno;
    }

    // @TYPE( POST )
    // @PATH( BASE_URL/polos/editar/id )
    public function editar($id) {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
            exit();
        }

        $p = new Polo();

        if (!empty($_POST['nome'])) {
            $nome = addslashes($_POST['nome']);
            $status = $p->editar($id, $nome);

            echo $status;
        }
    }

    // @TYPE( GET )
    // @PATH( BASE_URL/polos/info/id )
    public function info($id) {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
            exit();
        }

        $u = new Usuario();
        $p = new Polo();

        $data = array(
            'js' => 'info-polo.js',
            'css' => 'gerenciador.css',
            'usuario' => $u->getById($_SESSION['pLogin']),
            'polo' => $p->getById($id)
        );

        $this->loadTemplate('info-polo', $data);
    }

    // @TYPE( GET )
    // @PATH( BASE_URL/polos/excluir/id )
    public function excluir($id) {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
            exit();
        }

        $p = new Polo();
        //TODO: tratar retorno de status via AJAX
        $status = $p->excluir($id);
    }

}