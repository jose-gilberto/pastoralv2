<?php
class loginController extends controller {

    public function index() {
        $data = array(
            'css' => 'login.css',
            'js' => 'login.js',
            'error' => false
        );
        $u = new Usuario();

        if (!empty($_POST['usuario']) && !empty($_POST['senha'])) {
            $usuario = addslashes($_POST['usuario']);
            $senha = md5($_POST['senha']);
            $status = $u->logar($usuario, $senha);

            if ($status) {
                header("Location: ".BASE_URL."gerenciador/");
            } else {
                $data['error'] = true;
            }
        }

        $this->loadTemplate('login', $data);
    }

    public function esqueci() {
        $data = array(
            'css' => 'login.css',
            'js' => 'login.js'
        );

        $this->loadTemplate('esqueci', $data);
    }

}