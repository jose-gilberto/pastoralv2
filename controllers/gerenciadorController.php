<?php
class gerenciadorController extends controller {

    public function index() {
        if (empty($_SESSION['pLogin'])) {
            header("Location: ".BASE_URL."login/");
        }

        $u = new Usuario();
        $data = array(
            'css' => 'gerenciador.css',
            'js' => 'gerenciador.js',
            'usuario' => $u->getById($_SESSION['pLogin'])
        );

        $this->loadTemplate('gerenciador', $data);
    }

    public function sair() {
        unset($_SESSION['pLogin']);
        header("Location: ".BASE_URL."login/");
    }

}