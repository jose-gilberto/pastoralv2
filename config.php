<?php
require 'environment.php';

$config = array();

if(ENVIRONMENT == 'development'){
    //Ambiente de Desenvolvimento
    define("BASE_URL", "http://localhost/pastoralv2/");
    $config['dbname'] = 'db_pastoral';
    $config['host'] = 'localhost';
    $config['dbuser'] = 'root';
    $config['dbpass'] = '';
} else {
    //Ambiente de Produção
    define("BASE_URL", "http://localhost/pastoralv2/");
    $config['dbname'] = 'db_pastoral';
    $config['host'] = 'localhost';
    $config['dbuser'] = 'root';
    $config['dbpass'] = '';
}
//Variável de conexão
global $db;

try{
    $db = new PDO("mysql:dbname=".$config['dbname'].";host=".$config['host'].";charset=utf8", $config['dbuser'], $config['dbpass']);
} catch (PDOException $e){
    echo "ERROR: ".$e->getMessage();
    exit();
}