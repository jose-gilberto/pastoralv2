<?php
class Padroeiro extends model {

    public function adicionar($nome, $data){
        if (!$this->existe($nome)) {
            $sql = "INSERT INTO padroeiros (nome, data_festa) VALUES (:nome, :data_festa)";
            $sql = $this->pdo->prepare($sql);
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":data_festa", $data);
            $sql->execute();

            return true;
        } else {
            return false;
        }
    }

    public function listar() {
        $dados = array();

        $sql = "SELECT * FROM padroeiros ORDER BY nome";
        $sql = $this->pdo->query($sql);

        if ($sql->rowCount() > 0) {
            $dados = $sql->fetchAll();
        }

        return $dados;
    }

    public function existe($nome) {
        $sql = "SELECT * FROM padroeiros WHERE nome = :nome";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome", $nome);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

}