<?php
class Polo extends model {

    public function adicionar($nome) {
        if (!$this->existe($nome)) {
            $sql = "INSERT INTO polos (nome) VALUES (:nome)";
            $sql = $this->pdo->prepare($sql);
            $sql->bindValue(":nome", $nome);
            $sql->execute();

            return true;
        } else {
            return false;
        }
    }

    public function listar() {
        $dados = array();

        $sql = "SELECT * FROM polos ORDER BY nome";
        $sql = $this->pdo->query($sql);

        if ($sql->rowCount() > 0) {
            $dados = $sql->fetchAll();
        }

        return $dados;
    }

    public function getById($id) {
        $dados = array();

        $sql = "SELECT * FROM polos WHERE id = :id";
        $sql= $this->pdo->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $dados = $sql->fetch();
        }

        return $dados;
    }

    public function editar($id, $nome) {
        if (!$this->existe($nome)) {
            $sql= "UPDATE polos SET nome = :nome WHERE id = :id";
            $sql = $this->pdo->prepare($sql);
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":id", $id);
            $sql->execute();

            return true;
        } else {
            return false;
        }
    }

    public function excluir($id) {
        $sql = "DELETE FROM polos WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        return true;
    }

    public function existe($nome) {
        $sql = "SELECT * FROM polos WHERE nome = :nome";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome", $nome);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

}