<?php
class Usuario extends model {

    public function logar($usuario, $senha) {
        $sql = "SELECT * FROM usuarios WHERE usuario = :usuario AND senha = :senha";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":usuario", $usuario);
        $sql->bindValue(":senha", $senha);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $dados = $sql->fetch();
            $_SESSION['pLogin'] = $dados['id'];
            return true;
        } else {
            return false;
        }
    }

    public function getById($id) {
        $array = array();

        $sql = "SELECT * FROM usuarios WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetch();
        }

        return $array;
    }

}