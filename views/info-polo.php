<nav>
    <div class="nav-wrapper blue darken-4">
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a>Bem Vindo, <?=$usuario['nome'];?></a></li>
            <li><a href="<?=BASE_URL?>gerenciador/sair/">Sair</a></li>
        </ul>
    </div>
</nav>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="user-view center-align">
            <div class="background blue darken-3"></div>
            <a><div class="white-text"><i class="material-icons medium">account_circle</i></div></a>
            <a><span class="white-text name"><?=$usuario['nome'];?></span></a>
            <br>
        </div>
    </li>
    <li><a href="<?=BASE_URL?>gerenciador/">Dashboard</a></li>
    <li><a href="<?=BASE_URL?>agentes/">Agentes</a></li>
    <li><a href="<?=BASE_URL?>comunidades/">Comunidades</a></li>
    <li><a href="<?=BASE_URL?>padroeiros/">Padroeiros</a></li>
    <li><a href="<?=BASE_URL?>polos/">Pólos</a></li>
    <li><a href="<?=BASE_URL?>gerenciador/sair/">Sair</a></li>
</ul>
<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>

<div class="conteudo">
    <div class="row">
        <h3 class="light titulo blue-text text-darken-4">Pólos</h3>

        <div class="col s12 m12">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">Informações sobre o Pólo</span>
                    <p>Aqui estão as informações atuais do Pólo.</p>
                    <br>
                    <div class="row">
                        <div class="input-field col m6">
                            <input type="hidden" id="idPolo" value="<?=$polo['id']?>">
                            <input disabled value="<?=$polo['nome']?>" id="disabled" type="text" class="validate">
                            <label for="disabled">Nome do Pólo</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m12">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">Editar Pólo</span>
                    <p>Preencha os campos do formulário para editar o pólo.</p>
                    <br>
                    <div class="row">
                        <form class="col m12" method="post" id="formPolo">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="nome" type="text" class="validate" name="nome" required>
                                    <label for="nome">Nome do Pólo</label>
                                </div>
                            </div>
                            <button type="submit" class="waves-effect waves-light btn blue darken-3">SALVAR</button>
                            <button id="btnVoltar" type="button" class="waves-effect waves-light btn blue darken-3">LIMPAR</button>
                            <a class="white-text" href="<?=BASE_URL?>polos"><button type="button" class="waves-effect waves-light btn blue darken-3">VOLTAR</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>