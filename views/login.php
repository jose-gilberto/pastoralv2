<div class="row">
    <div class="col m6 blue darken-3 esquerda">
        <div class="row">
            <div class="col m12 center-align">
                <img src="<?=BASE_URL;?>assets/img/logo_pastoral.png" class="responsive-img" width="250">
                <h2 class="light white-text">Pastoral Familiar</h2>
                <p class="white-text">Desenvolvido pela Prism Agência Digital. Todos os direitos reservados.</p>
            </div>
        </div>
    </div>
    <div class="col m6 direita">
        <div class="row center-align">
            <h3 class="blue-text text-darken-3 light">Login</h3>
            <br>
            <form class="col m12" method="post">
                <div class="row">
                    <div class="input-field col m6 s12 offset-m3">
                        <input type="text" class="validate" id="login" name="usuario">
                        <label for="login">Usuário</label>
                    </div>
                    <div class="input-field col m6 s12 offset-m3">
                        <input type="password" class="validate" id="senha" name="senha">
                        <label for="senha">Senha</label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <p class="center-align col m12">
                        <a href="<?=BASE_URL;?>login/esqueci/" class="blue-text text-darken-3">Esqueceu sua senha?</a>
                    </p>
                </div>
                <br>
                <?php if ($error): ?>
                <div class="row">
                    <p class="col m12 center-align red-text text-darken-3">Usuário ou Senha estão incorretos!</p>
                </div>
                <br>
                <?php endif; ?>
                <br>
                <button type="submit" class="waves-effect waves-light btn blue darken-3">ENTRAR</button>

            </form>
        </div>
    </div>
</div>