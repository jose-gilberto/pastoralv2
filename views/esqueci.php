<div class="row">
    <div class="col m6 blue darken-3 esquerda">
        <div class="row">
            <div class="col m12 center-align">
                <img src="<?=BASE_URL;?>assets/img/logo_pastoral.png" class="responsive-img" width="250">
                <h2 class="light white-text">Pastoral Familiar</h2>
                <p class="white-text">Desenvolvido pela Prism Agência Digital. Todos os direitos reservados.</p>
            </div>
        </div>
    </div>
    <div class="col m6 direita">
        <div class="row center-align">
            <h3 class="blue-text text-darken-3 light">Recuperação de Senha</h3>
            <br>
            <form class="col m12">
                <div class="row">
                    <div class="input-field col m6 s12 offset-m3">
                        <input type="text" class="validate" id="login" name="usuario">
                        <label for="login">Usuário</label>
                    </div>
                    <div class="input-field col m6 s12 offset-m3">
                        <input type="email" class="validate" id="email" name="email">
                        <label for="email">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col m12">
                        <p class="center-align blue-text text-darken-3">* Verifique a caixa de entrada do seu email, lá encontrará um link para recuperação de conta.</p>
                    </div>
                </div>
                <br><br>
                <a href="<?=BASE_URL;?>login/" class="waves-effect waves-light btn blue darken-3">VOLTAR</a>
                <button type="submit" class="waves-effect waves-light btn blue darken-3">ENVIAR</button>
            </form>
        </div>
    </div>
</div>