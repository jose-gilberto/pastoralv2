<nav>
    <div class="nav-wrapper blue darken-4">
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a>Bem Vindo, <?=$usuario['nome'];?></a></li>
            <li><a href="<?=BASE_URL?>gerenciador/sair/">Sair</a></li>
        </ul>
    </div>
</nav>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="user-view center-align">
            <div class="background blue darken-3"></div>
            <a><div class="white-text"><i class="material-icons medium">account_circle</i></div></a>
            <a><span class="white-text name"><?=$usuario['nome'];?></span></a>
            <br>
        </div>
    </li>
    <li><a href="<?=BASE_URL?>gerenciador/">Dashboard</a></li>
    <li><a href="<?=BASE_URL?>agentes/">Agentes</a></li>
    <li><a href="<?=BASE_URL?>comunidades/">Comunidades</a></li>
    <li><a href="<?=BASE_URL?>padroeiros/">Padroeiros</a></li>
    <li><a href="<?=BASE_URL?>polos/">Pólos</a></li>
    <li><a href="<?=BASE_URL?>gerenciador/sair/">Sair</a></li>
</ul>
<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>