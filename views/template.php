<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Pastoral Familiar</title>
    <link rel="stylesheet" href="<?=BASE_URL;?>assets/css/materialize.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?=BASE_URL;?>assets/css/<?=$css;?>">
</head>
<body>

<?php $this->loadViewInTemplate($viewName, $viewData); ?>

<script type="text/javascript" src="<?=BASE_URL;?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?=BASE_URL;?>assets/js/materialize.js"></script>
<script type="text/javascript" src="<?=BASE_URL;?>assets/js/<?=$js;?>"></script>
</body>
</html>